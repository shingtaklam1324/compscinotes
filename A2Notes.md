# Computer Science A2 Notes

- [Computer Science A2 Notes](#computer-science-a2-notes)
  - [Data Representation](#data-representation)
    - [User Defined Data Types](#user-defined-data-types)
      - [Enumerated](#enumerated)
      - [Pointer](#pointer)
      - [Record](#record)
      - [Set](#set)
      - [Class/Object](#classobject)
    - [File Organisation](#file-organisation)
      - [Serial File](#serial-file)
      - [Sequential File](#sequential-file)
      - [Direct Access File](#direct-access-file)
    - [Floating Point Numbers](#floating-point-numbers)
  - [Communication and Internet Technologies](#communication-and-internet-technologies)
    - [Protocols](#protocols)
      - [Protocol Stack](#protocol-stack)
      - [The TCP/IP Protocol Suite](#the-tcpip-protocol-suite)
      - [Application Layer Protocols](#application-layer-protocols)
    - [Circuit Switching, Packet Switching and Routers](#circuit-switching-packet-switching-and-routers)
      - [Circuit Switching](#circuit-switching)
      - [Packet Switching](#packet-switching)
      - [Router and the Internet](#router-and-the-internet)
    - [Local Area Networks (LAN)](#local-area-networks-lan)
      - [Bus Topology](#bus-topology)
      - [Star Topology](#star-topology)
      - [Network Interface Cards (NIC)](#network-interface-cards-nic)
      - [Wireless Networks](#wireless-networks)
  - [Hardware](#hardware)
    - [Logic Gates and Circuit Design](#logic-gates-and-circuit-design)
      - [Half Adder and Full Adder](#half-adder-and-full-adder)
    - [Boolean Algebra](#boolean-algebra)
    - [Karnaugh Maps](#karnaugh-maps)
    - [Flip-Flops](#flip-flops)
    - [RISC and CISC](#risc-and-cisc)
      - [Interrupts](#interrupts)
    - [Parallel Processing](#parallel-processing)
      - [Parallel Architectures](#parallel-architectures)
      - [Parallel Computing](#parallel-computing)
  - [System Software](#system-software)
    - [Purposes of an Operating System (OS)](#purposes-of-an-operating-system-os)
      - [User Interfaces](#user-interfaces)
      - [Resource Management](#resource-management)
      - [Processor Management](#processor-management)
      - [Virtual Memory](#virtual-memory)
    - [Virtual Machine (VM)](#virtual-machine-vm)
    - [Translation Software](#translation-software)
      - [Interpreters](#interpreters)
      - [Stages of Compilation](#stages-of-compilation)
  - [Security](#security)
    - [Asymmetric Keys and Encryption Methods](#asymmetric-keys-and-encryption-methods)
      - [Symmetric Key Encryption](#symmetric-key-encryption)
      - [Asymmetric Key Encryption](#asymmetric-key-encryption)
    - [Digital Signatures and Certificates](#digital-signatures-and-certificates)
      - [Digital Signature](#digital-signature)
      - [Digital Certificate](#digital-certificate)
    - [Encryption Protocols](#encryption-protocols)
    - [Malware](#malware)
      - [Vulnerabilities](#vulnerabilities)
  - [Monitoring and Control Systems](#monitoring-and-control-systems)
    - [Overview of Monitoring and Control Systems](#overview-of-monitoring-and-control-systems)
      - [Loops and Sleeping](#loops-and-sleeping)
    - [Bit Manipulation](#bit-manipulation)

## Data Representation

### User Defined Data Types

In most (if not all) programming languages, the user has the ability to define new data types from existing ones. There are two types of user defined data types, composite and non-composite. Non-Composite data types are ones where the definition does not involve any other types (eg. enumerated), and Composite Data Types are ones that refers to (at least one) other data types (eg. Array, Record).

User defined data types are needed to make it possible to pass multiple pieces of data together with a record. Also, in more complex programs, a built in data type such has integer can represent different things (for example age, number of objects, year etc.), however the record type used to represent an object will be unique.

#### Enumerated

An enumerated type is a non-composite data type where all of the possible values are listed. For example, to represent the Months, an enumerated data type can be used.

```pseudocode
TYPE
TMonth = (Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec)

DECLARE ThisMonth : TMonth
ThisMonth <- Dec
```

This is useful when all of the possible options are already known, as it prevents invalid values from being used, for example `ThisMonth <- Hello` would be invalid.

Enumerated types are ordinal (can be put into increasing/decreasing order). Typically this is in the order in the declaration. So `Jan < Feb` would be `TRUE` whereas `Aug > Dec` would be `FALSE`.

#### Pointer

A pointer is a non-composite data type which is used to reference a location in memory. The pointer itself has a defined type, which refers to the type of the data stored in the address in memory. A pointer can be declared like so:

```pseudocode
TYPE
TIntPointer = ^INTEGER
```

Dereferencing is when the value stored in the memory location is accessed through the pointer. This is done like so:

```pseudocode
DECLARE Pointer : TIntPointer
ValuePointedTo = Pointer^
```

#### Record

A record data type is a composite data type that contains a fixed number of components known as fields, each has a name and a data type. An example record data type is like so:

```pseudocode
TYPE
Student
    DECLARE Name : STRING
    DECLARE Age : INTEGER
    DECLARE StudentID : INTEGER
ENDTYPE
```

To access a specific field of the record, a dot can be used.

```pseudocode
StudentName <- Student.Name
Student.Age <- Student.Age + 1
```

The field types of a record can be declared inline, so a Day type can look like so:

```pseudocode
TYPE
Day
    DECLARE Month : (Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec)
    DECLARE Day : 1 .. 31
```

#### Set

A set is a collection of items with the same data type where the items are not ordered, and that folow the laws of set theory from mathematics.

- Each item only appears in the set once
- Intersection of two sets (keep only items present in both sets)
- Union of two sets (keep items present in either or both sets)
- Difference of two sets (Items in A but not in B)

#### Class/Object

An object is like a record, but it has methods attached to the object. For example, using the student record example from above, we can write a method for when the student has a birthday (age + 1)

```pseudocode
PROCEDURE UpdateAge (BYREF Current : Student)
    Current.Age <- Current.Age + 1
ENDPROCEDURE

// To update the age of a student

UpdateAge(Bob)
```

Pseudocode doesn't have classes, so the following example is in Python.

```python
class Student(object):
    # Initialisation Method to create a new student object
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def updateAge(self):
        self.age = self.age + 1

Bob.updateAge()
```

### File Organisation

When storing the data for a computer program to use in a file, there are two methods of doing so, either store the data as text, like the JSON example below (not in the spec, just included for reference). This can be edited using any text editor and can be read by any program.

```json
{
    "name": "bob",
    "age": 13
}
```

Alternatively, the data can be stored in a binary file. When this is used, the representation of the data would be in a specific format that is defined by the program that uses it. If multiple pieces of data are to be stored, for example to be used as a database, then the data is stored in records, each of which contains fields.

There are multiple ways to order the records in the file, each with their own advantages and disadvantages.

#### Serial File

In a serial file, there is no defined order for the records. When a new record is added, it is added to the end of the file. This means that the records would be in chronological order. As there is no defined order, thie means that if a specific record is to be accessed, a linear search needs to be performed on the record, which can take a long time if the file size is large. However it takes the least amount of time to add a new file, and it is the quickest if every single record in the file is going to be read.

This means that serial files are the most useful when the data is in chronological order and/or the whole file needs to be read (batch processed). For example in a bank transaction log, every transaction is kept in the file in chronological order, and each individual record isn't very useful without the preceding ones. Also, as the logical order of the data is chronological, there is no need to sort the data every time a new record is added, as it would have already been in chronological order, and appending the new record is all that is necessary.

In a small file, serial file organisation can be the most appropriate as it is easy to append new records as the file won't need to be sorted. Also, the overhead from searching isn't significant on a small file.

#### Sequential File

A sequential file is one where the records stored in order, where the order is determined by a key field. The key field needs to be unique for each record in the file. This is the most similar to a database, and the principles are similar. When a record is looked up using its key field, the program searches through the records looking for a matching key field. This is faster than the search performed for a serial file as a binary search can be used given that the records are ordered. However, when adding a new record, the order needs to be maintained. This means that an insertion sort will be necessary, which makes adding a new record slower than in a serial file. Alternatively, sometimes a new file is created instead, copying the old file over, inserting the new record in the appropriate location.

A sequential file is useful for situations where data needs to be stored long term, as the time taken to add a new record isn't very significant and the lookup speeds on a sequential file are quite fast. In situations where the whole file needs to be read, the speed on a sequential file and serial file are the same. This is one of the benefits of using a sequential file, as it can be read sequentially, like in a serial file, but also directly, like in a Direct-Access File. Also, a sequential file can only be used in scenarios where there is a logical key field for each record.

#### Direct Access File

A direct access file (also known as a random (access) file) allows a record to be found immediately (in most cases) without the need to search through the file. This is done by hashing the key field for each record. A hash function is one that takes in a piece of data and outputs a number. The program then uses the hash generated to find the loocation in memory to store the data. However if two records have the same hash (known as a hash collision), then an overflow method is needed. Typically the next location available is used. This means that when looking for a record, a linear search may need to be used as the record may be in subsequent locations.

A direct access file is prefered over a sequential file when the file size is large, as the time taken to access a specific record is faster than in a sequential file. Also, if a file is going to be frequently accessed, then a direct access file is also preferred, as it reduces the amount of time to process the data.

### Floating Point Numbers

So far, we have only dealt with integers, non-negative ones at GCSE and two's complement at AS. To represent decimals is more complicated. To see why floating point is necessary, it makes sense to look at a fixed point representation first.

For example, using a fixed point, 7.875 would be represented like so

| -16 | 8   | 4   | 2   | 1   | .   | 0.5 | 0.25 | 0.125 |
| --- | --- | --- | --- | --- | --- | --- | ---- | ----- |
| 0   | 0   | 1   | 1   | 1   | .   | 1   | 1    | 1     |

However, this format cannot represent numbers greater than `15.875` and all of the numbers go up in increments of `0.125`. If we move the decimal point left one place, the maximum number representable decreases, but the increment is halved to `0.0625`. If we move the number right one place, then the opposite happens, the maximum increases, but so does the increment. So it would make sense to have the number be able to move the decimal place left or right depending on the application and the number being represented.

In a similar method to standard form, we can use base 2 to represent the numbers. Where 7.875 can be represented as `63 - 2^-3`. Equivalently, it can also be represented as `31.5 - 2^-2`. The first number is known as the mantissa (m), and the second (not 2) is known as the exponent(e). So the general form for a floating point number is `m - 2^e`, where both `m` and `e` are in two's complement.

So 7.875 can be represented as

```pseudocode
00111111. 11111101 (63        - 2^-3)
0011111.1 11111110 (31.5      - 2^-2)

...

0.0111111 00000100 (0.4921875 - 2^4)
```

Obviously, some representations make more sense than others to use. The process of maximising the precision of the number representes is known as normalisation. This works by moving the decimal point to the position where the two most significant bits are different (0.1 or 1.0, not 0.0 or 1.1). Therefore, 7.875 would be `0.1111110 00000011` in normalised floating point representation.

If we increase the number of bits used by the mantissa, then the precision of the numbers that are able to be represented is improved. If the number of bits used for the exponent is increased, then the range of numbers that can be represented is greater. Obviously in most real world scenarions the total number of bits is constant, so it is a compromise between the range and precision of the numbers that can be represented.

Floating point numbers are not a perfect representation for a lot of scenarios, as there are numbers that cannot be represented. For example, the decimal `0.1` cannot be represented exactly by a float. This error would then be carry forward as arithmetic is performed on the floating point numbers, for example `0.1 + 0.2 == 0.3` in Python evaluates to `false` as none of the numbers can be represented exactly as a float. Using the representation from above, the maximum value possible is represented as `0.1111111 01111111`, which means that numbers greater than this cannot be represented, this problem is known as overflow. The minimum non-negative value is `0.1000000 10000000`, and smaller numbers than this cannot be represented exactly. This is known as underflow, and the behaviour is not defined in this case. Some representations have a special code for this (most floating point representations in use now) or programs may replace it with either the smallest value or 0.

## Communication and Internet Technologies

### Protocols

A protocol is a set of rules for data transmission that is agreed by the sender and receiver. If there isn't a protocol, then there won't be a defined method to send the data and it is impossible (or very unlikely) that the data can be sent successfully across the network.

#### Protocol Stack

At the lower levels, sending an email and accesing a web page on the internet is very similar, but at the higher (application) level, the two are very different. It is preferable to not "reinvent the wheel" and the parts in common at the lower level and be used by both applications. This is also useful as the hardware (routers, gateways etc.) will only need to support one protocol. Then each application can use its own protocol that builds on the lower level protocol.

#### The TCP/IP Protocol Suite

The TCP/IP Protocol suite are a group of related protocols that are used for communication over the internet.

| Layer       | Example Protocols          |
| ----------- | -------------------------- |
| Application | HTTP, FTP, SMTP, DNS, POP3 |
| Transport   | TCP, UDP, SCTP             |
| Network     | IP, IGMP, ICMP, ARP        |
| Data Link   | Ethernet                   |

The protocol suite is named after the foundational protocols in the table above, TCP and IP.

Each layer in the suite has a specific role. The application layer handles communications within the same computer, and also creating and reading packets from other applications which may be on different computers. The transport layer handles communications from one computer to another, either on the local network or on a remote network connected by routers. The internet layer exchanges datagrams across networks, therefore connecting different networks together, creating the internet. The data link layer describes how the data is transmitted within the local network, including handling based on the network topology.

##### TCP (Transmission Control Protocol)

TCP is in the transport layer and allows for applications to communicate, and it is a session based protocol. This means that both the sender and receiver are connected during the entire transmission of data. Therefore TCP is responsible for establishing and also terminating connections. TCP is also responsible for congestion avoidance on packets, which means that if a path is too congested, then the packets will be diverted to another path. TCP also acknowledges the receipt of packets, and if a packet is sent but the acknowledgement is not received, then the packet will be retransmitted.

The TCP process is like so:

1. The server listens for a connection request
2. The client sends a connection request and waits for a connection request from the server
3. The server sends the connection request.
4. The connection is established and the data can start to be sent

The data is sent in datagrams (analogous to packets in IP), which contain the data to be sent as well as additional information which is used by TCP. This includes

- Source Port Number
- Destination Port Number
- Sequence Number
- Checksum

The checksum is used by the receiver to verify that the packet has been sent correctly and the data is valid. As packet switching may be used, the packets may not arrive at the receiver in the same order that they were sent in, therefore the sequence number allows the packets to be reassembled into their original order. The port number is similar to the IP address, but it refer to an address on a computer. Typically port numbers are assigned to specific protocols, for example 20 and 21 are FTP, 25 is SMTP and 80 is HTTP.

##### IP (Internet Protocol)

The network layer, in particular the IP is to ensure correct routing across the internet. It takes the packet from the transport layer and adds an additional header, which contains the IP address of the sender and receiver. It also contains the addressing method used by the network, for example subnetting or NAT. To find the IP address of the header, it uses the DNS system to find the address corresponding to an URL.

The IP packet, which is known as a 'datagram', is then sent to the data link layer and assembled into 'frames'. Then, the transmission of data can begin and the IP has no further duty. If IP receives a packet, then the inner contents is simply passed onto TCP without reading or processing it.

#### Application Layer Protocols

These are protocols that are part of the Application Layer on the TCP/IP protocol suite.

##### HTTP

Hypertext Transfer Protocol is the protocol that is used to transmit web pages across the internet. It is a client server protocol, where the web browser (client) makes requests to the server, which the server then sends a response. Resources served by HTTP are identified by their URL. For example, a client may send a request to `GET /pages/index.html` which the server then gets the `pages/index.html` file and sends a response containing the file requested. If there is no error, then a code of `200 OK` is returned. However, sometimes a different code is returned, for example `404 Not Found` or `403 Forbidden`.

##### FTP

FTP is the protocol that is used to transfer files between a client and  a server on a network. The principles of operation are similar to HTTP where the client makes a request and the server responds. The feature of FTP is that the control and transmission parts of FTP are separate, which makes the process of transmitting data across different operating systems easier. The user uses FTP by sending commands to the server, which then executes those commands. In addition, the user can access files on the server. This can be done without identifying themselves to the server, which means that FTP can be anonymous.

##### SMTP and POP3

SMTP and POP3 are protocols responsible for transmission of emails. SMTP is known as a 'push' protocol, where the message is pushed from the client to the server. This is used to send the email from the sender's email client to their email server, and across the internet to the receiver's email server. To access the email, the POP3 protocol is used. POP3 is a 'pull' protocol, and the receiver's email client has to actively request for the email from their email server and download it, as it is not automatically sent from the server like with SMTP.

Additional information present in an email packet include the IP addresses of the sender and receiver, the sequence number as well as the checksum of the packet.

##### BitTorrent

BitTorrent is an example of what is known as a peer-to-peer network. In a client-server model, all of the computers are either clients that request a service or servers that provide a service. In a peer to peer (P2P) model, all of the computers will request and provide services to the other computers.

When sharing a file on a P2P network using BitTorrent, there are special names for computers that have certain roles/properties.

- Trackers are central servers that have the details of all of the computers in the swarm, which allows a new computer to join the P2P network.
- Seeds are computers that have 100% of the file that is being shared and is only uploading content
- The Swarm is all of the computers connected to the P2P network that have some/all of the file.
- A leech is a computer that downloads much more of the file than they upload

To share a file using BitTorrent, the following steps is the process required:

- BitTorrent client software made available
- One computer must keep a complete copy of the torrent/file to be shared
- Torrent/file is split into small pieces
- A computer joins (a swarm) by using the BitTorrent software to load a torrent descriptor file  
- The computer can now download a piece of the file
- Once a computer has a piece it can become a seed and upload (to other members of the swarm)
- Pieces of the torrent are both downloaded and uploaded (by each member of the of the swarm)
- A server called a tracker keeps records of all the computers in the swarm
- The tracker shares their IP addresses allowing them to connect to each other

##### An example packet

The following is what an HTTP Response/TCP/IP frame would look like. Extra information has been replaced by `...`

```n/a
--- IP

    [IP Version]...[Total Length]
    ...[Flags][Fragmentation Offset]
    [TTL][Protocol][Header Checksum]
    [Source IP Address]
    [Destination IP Address]

--- IP Payload (TCP)

    [Source Port][Destination Port]
    [Sequence Number]
    ...
    [Checksum]...

--- TCP Data (HTTP)

    HTTP/[HTTP Version] [Code] [Status]
    ...
    [Headers]
    ...
    Content-Type: text/html

    [Data]
```

Flags and fragmenatation offset refer to the fact that sometimes an IP datagram needs to be split into several to be able to be transmitted. The flags alter how this is done and the fragmentation offset is used to reassemble the packet.

Time to Live (TTL) refers to the number of 'hops' between server/routers before the packet is dropped. This is used as otherwise there can be packets that will never be dropped if the destination is not reachable. The Protocol number refers to the transport layer protocol the packet uses, for example 6 is TCP.

### Circuit Switching, Packet Switching and Routers

#### Circuit Switching

Circuit switching is the process where a dedicated link between the two ends of communication is used. This is the process used in traditional telephone systems. The process begins by the sender requesting for a connection to the receiver. The system checks if the receiver is ready, if so, a dedicated connection is established. Then the data is transferred. After the data has completed transferring, the link is removed.

This has the benefit of having the lowest latency as the lines are not used by other data, and the data can be transmitted immediately and uninterrupted. There is also no chance of packet loss like in a Packet switched system. However, if the data does not require all of the bandwidth available on the lines then the extra bandwidth is wasted, as each line can only be used by one connection.

One situation where it would be beneficial to use Circuit Switching is in real time communications, for example video conferencing. This is because packets are much less likely to be dropped, which can cause interruptions. Also, communications (esp. video and audio) can become out of sync if packets get delayed in transmission. Finally, circuit switching means that the data gets the highest priority and won't be delayed in transmission.

#### Packet Switching

To reduce the wastage of bandwidth on a network, packet switching is used on most networks now such as the internet. This functions by having a large number of nodes (routers) and data can be sent from one node to another. The data to be sent is broken down into small pieces known as 'packets' and sent. The node then examines the address of the recipient of the data and forwards the data to another node closer to the recipient. This process is repeated until the data has reached the intended recipient.

This has the benefit of reducing wasted bandwith as the same lines can be used by a large number of connections. However there is no guarantee that the data will arrive in order like in a circuit switched network. Also, if one line is congested then all of the packets travelling through that line will be slowed down. Finally, there is a chance that the packet will never make it to the intended recipient, where the packet is then 'dropped'. This is the reason for the sequence number, congestion avoidance and TTL built into the TCP protocol.

Another benefit of packet switching is that if an alternative route is made available, packet switching can take advantage of this, but circuit switching cannot. Also, if some of the data is lost in transmission, then only the lost packets need to be resent.

#### Router and the Internet

A router is a device that takes a packet, examines the headers and then forwards the data onto another device. The ability to decide which device to forward the data to is essential for the functionality of the internet, as if the routers were replaced with switches, then the route the data travels through needs to be planned out ahead of transmission. The router allows this to be done dynamically.

When the router receives a packet, it examines the packet's IP header to look at the destination IP address. The router has a routing table, which it can use in addition to information such as congestion on the network to decide which device to forward the packet to. Information in the routing table include the IP address of the distination, metrics to determine the route to take, the sub net mask to use and the IP addresses of possible next hops. The TTL in the IP header is decremented by 1 to ensure that the packet doesn't get stuck on the network forever. Then the packet is sent to the next router which repeats this same process until the data has reached its intended recipient or the packet's TTL is 0.

### Local Area Networks (LAN)

#### Bus Topology

The simplest (and cheapest) network topology is known as a bus topology, in which all of the computers are connected to a single cable, known as the bus.

```n/a
       ┌─────┐    ┌─────┐    ┌─────┐    ┌─────┐  
       │  A  │    │  B  │    │  C  │    │  D  │  
       └──┬──┘    └──┬──┘    └──┬──┘    └──┬──┘  
┌───┐     │          │          │          │     ┌───┐
│ T ┝━━━━━┷━━━━━━━━━━┷━━━━━━━━━━┷━━━━━━━━━━┷━━━━━┥ T │
└───┘                                            └───┘
```

The `T` at the ends of the cable are known as terminators. Although the bus topology is the simplest and easiest to implement, it is not used very much now as there is a major downside to the bus topology. When a computer is transmitting data to another computer, the bus will be in use, and the other computers cannot transmit any data without corrupting the data. Also, the data transmitted on the bus can be read by any of the computers on the bus.

To avoid the collision issue, a process known as CSMA/CD is in use. This is the principle where if a computer would like to transmit data and the bus is in use, then it would wait and check regularly for if the bus is in use, and when it is free transmit the data. However if all of the computers wait for the same delay, then all will check the bus, find that it is not in use and transmit at the same time, corrupting the data. Therefore the delay that each computer waits between checking the bus is random, meaning it is very unlikely that multiple computers will be checking and transmitting at the same time.

#### Star Topology

To fix many of the issues that come with the Bus topology, a Star Topology is now much more common. With a star topology, every device is connected to a central device, which can be a hub, switch or a router.

```n/a
┌─────┐                    ┌─────┐
│  E  ├──────┐      ┌──────┤  F  │
└─────┘      │      │      └─────┘
┌─────┐    ┌─┴──────┴─┐    ┌─────┐
│  D  ├────┤  Router  ├────┤  A  │
└─────┘    └─┬──────┬─┘    └─────┘
┌─────┐      │      │      ┌─────┐
│  C  ├──────┘      └──────┤  B  │
└─────┘                    └─────┘
```

As each device is only connected to the router by a single wire, the data cannot be corrupted by different devices sending at the same time. This also means that if A was sending a message to C, D can simultaneously send a message to F and the two messages wouldn't corrupt. This does not however fix the issue of a single point of failure from the bus topology, where if the bus fails, the whols system fails. In a star topology, if the router (or central device) fails, then the whole network is disconnected. The devices on the outside can be connected together, to form what is known as a Mesh network, which is (roughly) the architecture of the internet.

#### Network Interface Cards (NIC)

A NIC is a device that is required for end-devices (such as computers) on an Ethernet network. Each one has an unique physical address known as a MAC address. A NIC can be a physical extension card in older computer systems and when high performance is required, but more commonly it is integrated into the motherboard on modern systems.

#### Wireless Networks

There are three main types of wireless connections that are in use today. Bluetooth, WiFi and Cellular networks. Bluetooth networks are ad-hoc and the topology and structure depends on the application. Cellular is the mobile phone networks in use, and they are based around base stations which the carrier installs throughout the world.

WiFi is a wireless LAN network which has a star topology, and is centred around the wireless access point, which communicates with devices connected on the network wirelessly as well as having a physical connection to the internet (via Ethernet).

In a wireless network, the packets are transmitted using EM waves, which means that the data can be read by any computer. It also means that every computer must read all of the packets being transmitted by the access point to determine if it is for itself.

## Hardware

### Logic Gates and Circuit Design

#### Half Adder and Full Adder

TODO: Half and Full Adder

### Boolean Algebra

FIXME: Boolean Algebra

Boolean Algebra are a set of laws which can be used to manipulate

### Karnaugh Maps

TODO: Karnaugh Maps?

### Flip-Flops

TODO: Flip-Flops

One issue of the SR flip-flop is that there are specific input combinations (S=0 R=0 for a NAND SR, S=1 R=1 for a NOR SR) where both the outputs are the same. This is an invalid state as the outputs are supposed to be complements of the other. The SR Latch is then unstable.

To fix this issue, a JK flip-flop can be used. A JK adds in a clock pulse, which can be used to synchronise different flip-flops in a more complex circuit, also it prevents an invalid state from being entered on a JK flip-flop, as all 4 input combinations are valid. The invalid input scenario for an SR flip-flop simply toggles the output of the JK Latch.

Flip-flops are useful as a memory component, as each one can be used to store a single bit.

### RISC and CISC

There are two main ways of creating a CPU's control unit, either create it using logic gates, which is known as a hard-wired solution, as the control unit cannot be changed in any way later. The alternative is to use microprogramming, where there is a ROM chip which contains the microcode, which is the individual steps for an instruction.

The choice of control unit depends on the number and complexity of the instructions that are part of the CPU's instruction set. There are two main classes of CPUs, Reduced Instruction set and Complex Instruction Set. The latter (CISC) is the one in use on desktops, laptops and a majority of servers, where as (RISC) is in use on mobile devices and embedded devices. An overview of the differences between the two is below

| RISC                                            | CISC                                      |
| ----------------------------------------------- | ----------------------------------------- |
| Fewer instructions                              | More instructions                         |
| Simpler instructions                            | More complex instructions                 |
| Small number of instruction formats             | Many instruction formats                  |
| Predominantly Single-Cycle Instructions         | Multi-Cycle instructions                  |
| Fixed Length Instructions                       | Variable Length Instructions              |
| Only load and store instructions address memory | Many types of instructions address memory |
| Fewer Addressing Modes                          | More addressing modes                     |
| Large number of registers                       | Less registers                            |
| Hard-wired Control Unit                         | Microprogramming                          |
| Pipelining is easier                            | Pipelining is more difficult              |
| More RAM                                        | More Cache                                |

One of the major reasons for using a RISC processor is the ability to pipeline instructions. Consider an instruction to increment the value in ACC. The other registers in the CPU are not being used, so its possible to begin to fetch the next instruction as that one is executing.

Pipelining is instruction level parallelism, where depending on the number of stages per instruction, it is possible to execute multiple instructions at the same time.

Consider an example system where each instruction has 5 steps:

1. Instruction Fetch (IF)
2. Instruction Decode (ID)
3. Operand Fetch (OF)
4. Instruction Execute (IE)
5. Result Write Back (WB)

The CPU can execute the instructions in sequence like so:

| Clock Cycles -> | 1   | 2   | 3   | 4   | 5   | 6   | 7   |
| --------------- | --- | --- | --- | --- | --- | --- | --- |
| IF              | 1   |     |     |     |     | 2   |
| ID              |     | 1   |     |     |     |     | 2   |
| OF              |     |     | 1   |
| IE              |     |     |     | 1   |
| WB              |     |     |     |     | 1   |

But much of the CPU resources would be wasted as nothing is being done for the majority of the time. If pipelining is used, then it would look more like this:

| Clock Cycles -> | 1   | 2   | 3   | 4   | 5   | 6   | 7   |
| --------------- | --- | --- | --- | --- | --- | --- | --- |
| IF              | 1   | 2   | 3   | 4   | 5   | 6   | 7   |
| ID              |     | 1   | 2   | 3   | 4   | 5   | 6   |
| OF              |     |     | 1   | 2   | 3   | 4   | 5   |
| IE              |     |     |     | 1   | 2   | 3   | 4   |
| WB              |     |     |     |     | 1   | 2   | 3   |

Here, the processor will be able to handle more instructions in the same time and would then execute the program faster than without pipelining.

#### Interrupts

In a CISC program, if the program is interrupted, then there is only one instruction in the processor where the state needs to be stored. However, in a RISC program, there will be multiple instructions in execution that need to be handled. One option is to erase the contents in the pipeline (as no changes to register contents occur in steps 1 to 4, only in step 5 (WB) does the contents of registers change). For example, if it occured on Clock Cycle 6 of the table above, then it would simply store the state of the instruction 2 like in CISC and clear the contents of the other stages. Another option is to construct each part of the processor with its own Program Counter (the instructions may not be sequential (eg. jump)), which allows the whole processor to be stored and recovered once the interrupt has been handled.

### Parallel Processing

#### Parallel Architectures

Depending on the number of instructions being executed at once (single/many) and the amount of data being processed at once (single/many), there are 4 types of architectures.

- SISD (Single Instruction Single Data Stream)
- SIMD (Single Instruction Multiple Data Streams)
- MISD (Multiple Instruction Single Data Stream)
- MIMD (Multiple Instruction Multiple Data Stream)

SISD has just a single processor so there is no processor parallelism. SIMD is how an array processor functions, so there are multiple processing cores, each with their own data stream, but they all execute the same instructions. MISD is not implemented in commercial products. MIMD is where there are multiple processors each with their own data stream, and each executing a different instruction.

#### Parallel Computing

Example of a parallel computer system is known as a massively parallel computer, or a supercomputer, and these are used by organisations for highly computationally intensive applications.

There are technical issues however to parallel computer systems, both within processors (multi-core setups) as well as in multi-computer setups, which means that the performance of the overall system is often worse than the sum of the individual components. First of all, there is the hardware difficulty in passing the data between different processors, as this requires bandwidth, and the time taken to transmit this data also reduces performance. Also, the software has to be able to take advantage of the additional processing power, so if it is limited by external factors (eg IO), then parallism wouldn't improve the performance significantly. Additionally, programs written for parallel computer systems need to be careful as to not access the same parts of memory on different cores at the same time, as writing to it can lead to data races and unpredictable behaviour.

## System Software

### Purposes of an Operating System (OS)

A high level overview of the facilities provided by an operating system is discussed at AS Level, some of which will be discussed in greater depth below.

#### User Interfaces

The details of this are discussed in depth at AS Level, and very little is additional. The addition is the discussion about APIs.

When a computer program needs a specific functionality from the operating system, it performs a system call, which is a function that is defined in the kernel of the OS. The OS then handles the system call and then returns control to the program. Examples include printing to the screen and interacting with IO. More advanced capabilities are provided in what is known as APIs, or application programming interface. These are a group of functions that are provided by the OS to perform certain tasks, too complex to be a single system call.

#### Resource Management

One of the main roles of the operating system is to manage the resources available to the computer system. This relates to the CPU, Memory and the I/O available to the computer.

#### Processor Management

Multi-tasking is the principle by which multiple programs can be executed at the same time by running parts of each program then switching to another program. A process is a program that is in memory and has a Process Control Block (PCB) that contains the information relevant to the execution of the process.

Schedulers are needed to decide which process to execute and for how long. This allows for multi-programming, which is having multiple programs run simulatneously, as well as allowing higher priority tasks to be completed first. Another reason for having a scheduler is to reduce the amount of CPU time that is wasted. There are three main types of schedulers. A long term scheduler, which is responsible for selecting the programs that are going to be executed by moving them from disk to main memory. A medium term scheduler is responsible for moving programs from memory back to the disk when the memory is getting full. A short term scheduler decides when a process has access to the CPU.

A process can be in a number of states, the outline of the different states and the transitions between them are outlined below. Also note that a process can be split into different parts, each of which is known as a thread.

![alttext](processControl.png)

As it is shown in the diagram, interrupts can halt the execution of the existing program in the running state. This can come from a variety of sources:

- External Interrupts
- Scheduler decides to halt the execution
- I/O Operation required by running program

In the case where the scheduler halts the execution of the process, the kernel of the OS is the interrupt handler used. The scheduler halts the program when the program reaches the end of its time slice, which is the time allocated for the process to run.

##### Interrupt Handling

Interrupt handling in processors follow a process that looks like so:

1. Disable interrupts
2. Save current task
3. Identify source of interrupt
4. Jump to Interrupt Service Routine
5. Restore Task
6. Enable Interrupts

The purpose of stage 1 is to ensure that the interrupt handler is not itself interrupted. In a system where there are interrupt priority levels, then this step would typically only disable interrupts with a lower priority than the one currently being processed.

Step 2 saves the current task by copying the content of the registers to the stack. Step 5 can then restore the task by popping off the top of the stack.

Step 3 is required as different interrupts will require different ISRs to handle.

Step 6 is necessary so that other interrupts (of equal or lower priority) can be processed after the current one has finished processing.

#### Virtual Memory

As the amount of memory available to a computer is much smaller than the amount of hard disk space available, there are cases where the amount of memory available is not sufficient. Therefore it would be required to move parts of the memory back to the disk when it is not being used to free up space. The way that this is done is known as paging. This splits the memory into much smaller parts (around 4K), each known as a page. Each page can be moved from memory to the disk and vice versa.

When a process in the running states needs a part of memory, one of two things can happen. Either the page is already in memory, and the process continues. If the page is on disk, then the page will need to be loaded from memory. If there is free space in memory, then the page can be loaded directly. However if the memory is full, then a page will need to be swapped. There are multiple algorithms that can be used to determine which page to swap, including first added, last added, last used, least recently used.

However, this comes with an overhead. First of all, information about each page will need to be stored, which can reduce the amount of memory available. Also, if an inefficient swap algorithm is used then an issue known as thrashing can occur, which is where the same page is loaded and unloaded from memory over and over again.

### Virtual Machine (VM)

A virtual machine uses software to emulate a hardware setup. It can be used to run an operating system within an exisiting one. Each virtual machine requires a handler, which translates the system calls and IO requests from the guest system to the host OS and also manages the resources available to each VM. A virtual machine monitor can handle multiple VMs at once, and the requirements for this include the ability to create and delete VMs and to separate the VMs, preventing one from being able to make changes in another.

The Operating System being emulated is known as a Guest OS, and the OS actually running on Physical hardware (and the VM software is running in) is known as the Host OS.

Benefits of a VM are that it can provide a sandboxed environment where changes inside the VM does not affect the host system. Also, it allows for operating systems where the hardware required isn't available to be executed by emulating the hardware. It also reduces the setup time and cost required for a test system, which allows more combinations and variations to be tested in a shorter period of time. However, the performance of the VM will be reduced compared to a system running on native hardware. This means that benchmarking and testing the actual performance of software accurately may be difficult. Also, it is not possible to emulate some hardware.

### Translation Software

#### Interpreters

An interpreter can execute a program line by line by reading each line, parsing the line, translating the parsed version to an intermediate form and then it is executed. More detail of this is in the AS Level and is not repeated here.

#### Stages of Compilation

The process of compiling a program can be considered to be 4 stages:

1. Lexical Analysis
2. Syntax Analysis
3. Code Generation
4. Optimisation

Lexical analysis is the process of reading the source code of a program and generating a list of tokens. For example, the Python code below

```py
print("1 + 1 is", 1 + 1)
```

would be lexed into

| Token Type | Value       |
| ---------- | ----------- |
| Identifier | `print`     |
|            | `(`         |
| String     | `1 + 1 is"` |
|            | `,`         |
| Integer    | `1`         |
| Operator   | `+`         |
| Integer    | `1`         |
|            | `)`         |

Syntax Analysis would take the list of tokens and generates a parse tree, which represents the structure of the program and the grammar of the language. For example, the following can be one representation of what the parse tree would look like (indentation represents levels in tree):

```ast?
function_call
  - name: print
  - parameters:
      - "1 + 1 is"
      - add:
        - 1
        - 1
```

Code generation takes the parse tree and generates assembly code or byte code depending on the language. The Python example is compiled by the CPython bytecode compiler to:

```py_bytecode
2           0 LOAD_GLOBAL              0 (print)
            2 LOAD_CONST               1 ('1+1 is')
            4 LOAD_CONST               2 (1)
            6 LOAD_CONST               3 (1)
            8 BINARY_ADD
           10 CALL_FUNCTION            2
```

The final stage is optimisation, which takes in the generated code and improves the efficiency of the generated code. There are a variety of methods, one is to perform computation of constants at compile time. Another is to reduce redundancies in the code. Reordering instructions can also reducte repetition, and may be necessary to take advantage of pipelining. The above example would be optimised to:

```py_bytecode
2           0 LOAD_GLOBAL              0 (print)
            2 LOAD_CONST               1 ('1+1 is')
            4 LOAD_CONST               2 (2)
            6 CALL_FUNCTION            2
```

2 instructions were removed by the compiler performing the computations and simply putting the result in, instead of performing the same calculations when the program runs.

FIXME: BNF and RPN?

## Security

### Asymmetric Keys and Encryption Methods

Encryption is the process of taking information in plain text and transforming it into ciphertext, which can not be easily read. The inverse process is known as decryption. The main concerns for why data is encrypted are as follows:

- Confidentiality, only the intended recipient should have access to the content of the message
- Authenticity, the recipient is certain who sent the ciphertext
- Integrity, the message should not be modified during transmission. If it is, it should be detectable

There are two main forms of encryption. Symmetric key and asymmetric key.

#### Symmetric Key Encryption

Symmetric key is the simpler one of the two. There is one key which is both used to encrypt and decrypt the information. The process looks like so:

```haskell
-- Sender's side
plaintext = "..."
ciphertext = Encrypt(plaintext, key)

Send(ciphertext)

-- Receiver's side
ciphertext = Receive()
plaintext = Decrypt(ciphertext, key)
```

This process guarantees Confidentiality, Authenticity and Integrity if the key has been transmitted securely and is sufficiently complex.

The issue here is that although the process is simple, transmitting the key to the receiver without it being intercepted requires a secure method of its own.

#### Asymmetric Key Encryption

Asymmetric key encryption solves this issue by using two keys which are related. A public key which is made available to everyone, and a private key that is only available to one person. The message can be encrypted using either key, but it can only be decrypted using the other key. To send a confidential message, it would look like so:

```haskell
-- Sender's Side
plaintext = "..."
RequestPublicKey()

-- Receiver's Side

-- If a session key is used, instead of a constant private-public key pair
privatekey, publickey = GenerateKeyPair()

Send(public_key)

-- Sender's Side
publickey = Receive()
ciphertext = Encrypt(plaintext, publickey)

Send(ciphertext)

-- Receiver's Side
ciphertext = Receive()
plaintext = Decrypt(ciphertext, privatekey)
```

The above guarantees Confidentiality and Integrity, but it does not guarantee authenticity. Using Asymmetric key encryption, it is also possible to guarantee Authenticity and Integrity for sending a verified message to the public. The process looks like so:

```haskell
-- Sender's Side
plaintext = "..."
ciphertext = Encrypt(plaintext, privatekey)

-- Assume that public key is already widely available
Send(ciphertext)

-- Receiver's Side
ciphertext = Receive()
plaintext = Decrypt(ciphertext, publickey)
```

Combining both processes can allow for a message to satisfy all three criteria for Confidentiality, Integrity and Authenticity, although it is not very commonly used.

### Digital Signatures and Certificates

#### Digital Signature

A cryptographic hash function is one that takes in some input, and then generates a number. It is not one-to-one, which means that knowing the output it is not possible to find the input. Also, there is no pattern to the output, so changing the initial message will produce an unpredictable change to the output. Using a cryptographic hash, it is possible to create a digital signature that verifies the integrity and authenticity of the message. The process looks like so:

```haskell
-- Sender's Side
message = "..."
digest = Hash(message)
digitalSignature = Encrypt(digest, privatekey)

Send(message)
Send(digitalSignature)

-- Receiver's Side

message = Receive()
digitalSignature = Receive()
senderHash = Decrypt(digitalSignature, publickey)
recvedHash = Hash(message)

-- if senderHash = recvedHash then the message has not been tampered.
```

#### Digital Certificate

However, digital signatures are not completely secure. As if the public key of the sender is forged, then it is possible to intercept the message and replace the message and digital signature with a different private key, and it won't be detected. Therefore Digital Certificates are required.

A digital certificate is a document to prove ownership of a public key, or it can be used to prove that the data is from a trusted source. A digitial certificate is issued by a Certification Authority (CA), which are trusted individuals/companies. An example process to generate a certificate looks like so:

```haskell
-- Person requiring Cert
Send(persons_public_key)
Send(additional_info)

-- CA

persons_public_key = Receive()
additional_info(Receive)
signature = Encrypt(persons_public_key, additional_info, ca_private_key)
cert = (person_public_key, additional_info, signature)
Send(cert)

-- Person

cert = Receive()
```

The digital signature is added to the certificate to authenticate and verify the certificate, as well as verifying that it is genuine.

The additional information included on a certificate include:

- Serial Number
- Name of CA
- Name of organisation owning certificate
- Date of expirary

### Encryption Protocols

SSL/TLS are encryption protocols that are used in the internet. TLS is the newer version that replaces the use of SSL in the vast majority of scenarios now. The HTTPS protocol uses SSL/TLS for encryption. To setup a secure connection using SSL/TLS, the process looks like so:

1. Browser requests that the server identifies itself.
2. Server sends a copy of its SSL Certificate and its public key.
3. Browser checks the certificate against a list of trusted Certificate Authorities.
4. If the browser trusts the certificate, it creates, encrypts and sends the server a symmetric session key using the server’s public key.
5. Server decrypts the symmetric session key using its private key.
6. Server sends the browser an acknowledgement, encrypted with the session key.
7. Server and browser now encrypt all transmitted data with the session key.

### Malware

Malware, or malicious software, are programs that have been introduced to a system for a harmful purpose. There are also other threats to computer systems. A list of common threats and solutions is below:

- Spam
  - Unsolicited emails containing advertising material sent to a distribution list
  - Problem: Fills up email servers with unwanted mail
  - Solution: Use an email filter that can delete spam email
- Worm
  - A standalone piece of malicious software that can reproduce itself automatically on the host computer as well as across a network
  - Problem: Can corrupt the user's computer and the data on the computer. It can also use up network bandwidth.
  - Solutions: Use anti-virus software, keep OS up-to-date
- Phishing
  - Attempt to obtain somebody’s confidential data or install malware through email
  - Problem: Loss of personal info
  - Solution: Email filters, do not click links in emails
- Pharming
  - malicious software or where a domain name or proxy server is compromised so that the user is misdirected to a fraudulent web site without their knowledge
  - Problem: Loss of personal info
  - Solution: Do not install unknown software, Verify digital signatures when using HTTPS online.
- Virus
  - Software that replicates by inserting itself into another piece of software or other data
  - Problem: Computer may stop functioning if storage space is full. Also the virus may have side effects apart from self-replication.
  - Solution: running anti-virus software
- Spyware/Keylogger
  - A piece of software that records/stores user actions/keystrokes without the user’s knowledge and sends them to a third party for analysis
  - Problem: It could read passswords and other personal info
  - Solution: Anti-virus

#### Vulnerabilities

Vulnerabilities that can be exploited include

- Not up to date anti-virus software
- Anti-virus software not regularly used and checking for malware
- Outdated OS
- Inadequate/No firewall
- Clicking on suspicious attachments/links
- Viewing websites with an out of date digital certificate

## Monitoring and Control Systems

### Overview of Monitoring and Control Systems

A monitoring system is a computer system that uses sensors to read its surroundings. A control system also uses actuators to cause changes in the environment.

Example Sensors include: Temperature, Moisture, pH etc. and example actuators include motors, heaters, lights etc.

The input information from sensors tend to be analog data, which needs to be put through an analog to digital converter (ADC) before the computer system can process it. Actuators tend to take in analog control signals, so a digital to analog converter (DAC) is needed to process the computer's output.

A processor is necessary to process the incoming data. This can involve various parameters that are set by the system designer which the processor then compares the incoming data to to decide on its actions. Parameters can include maximum/minimum acceptable values, sampling rate and tolerance. Alternatively in a monitoring system, a data logger connected to a storage device can be used to record readings from the sensor for later processing.

When a control system activates its actuators, the environment being controlled will change. Therefore the input of the sensors will change. This is known as feedback. This process is continuous as the change in input then causes a change in output.

#### Loops and Sleeping

For a monitoring or control system, the program will consist of an infinite loop that checks the sensors' values and act on this. As modern processors are much faster than the sensors, as well as any changes in the environment needing several (thousands or more) clock cycles to take effect, if the program checks the value of the sensor every cycle the processor resource is being wasted. Also, some sensors may be more important than others and need to be processed first.

Sleeping is a term that refers to an loop which the program waits for a predefined amount of time before executing the next instruction. It typically consists of a for loop with a large number of interations and no body.

```pseudocode
FOR i <- 1 TO 100000 DO
ENDFOR
```

An alternative process is to use interrupts to process and read sensor data. This has the benefit of processing the data as soon as it is made available, as well as the processor only needing to check the values of the sensors when an interrupt occurs.

### Bit Manipulation

FIXME: Add Bit Manipulation content?
